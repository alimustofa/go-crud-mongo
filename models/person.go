package models

import (
	util "gitlab.com/alimustofa/go-crud-mongo/utils"
	"gopkg.in/mgo.v2/bson"
)

// Person struct
type Person struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"`
}

// All func
func (p Person) All() (interface{}, error) {
	var session, db, err = util.Connect()

	if err != nil {
		panic(err)
	}
	defer session.Close()

	var people []Person
	errQuery := db.C("peoples").Find(bson.M{}).All(&people)

	if errQuery != nil {
		return nil, errQuery
	}

	return people, nil
}

// Get func
func (p Person) Get(id int) (interface{}, error) {
	var session, db, err = util.Connect()

	if err != nil {
		panic(err)
	}
	defer session.Close()

	var person Person
	errQuery := db.C("peoples").Find(bson.M{"id": id}).One(&person)

	if errQuery != nil {
		return nil, errQuery
	}

	return person, nil
}

// Create func
func (p Person) Create(person Person) (interface{}, error) {
	var session, db, err = util.Connect()

	if err != nil {
		panic(err)
	}
	defer session.Close()

	var collection = db.C("peoples")
	errInsert := collection.Insert(person)

	if errInsert != nil {
		return nil, errInsert
	}

	return person, nil
}

// Update func
func (p Person) Update(id int, changes interface{}) (interface{}, error) {
	var session, db, err = util.Connect()

	if err != nil {
		panic(err)
	}
	defer session.Close()

	var collection = db.C("peoples")
	info, errUpdate := collection.Upsert(bson.M{"id": id}, bson.M{"$set": changes})

	if errUpdate != nil {
		return nil, errUpdate
	}

	return info, nil
}

// Delete func
func (p Person) Delete(id int) (interface{}, error) {
	var session, db, err = util.Connect()

	if err != nil {
		panic(err)
	}
	defer session.Close()

	var collection = db.C("peoples")
	errDel := collection.Remove(bson.M{"id": id})

	if errDel != nil {
		return nil, errDel
	}

	return nil, nil
}
